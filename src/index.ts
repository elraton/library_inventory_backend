import "reflect-metadata";
import { createConnection } from "typeorm";
import * as express from "express";
import * as bodyParser from "body-parser";
import * as helmet from "helmet";
import * as cors from "cors";
import routes from './routes/index';
import { User } from "./entity/User.entity";
import { Brand } from "./entity/brand.entity";
import { Category } from "./entity/category.entity";
import { Product } from "./entity/product.entity";
import { Store } from "./entity/store.entity";
import { Outproduct } from "./entity/outproduct";
import { Itemoutproduct } from "./entity/itemout";

createConnection({
    type: "mysql",
    host: "127.0.0.1",
    username: "root",
    password: "root",
    database: "libreria",
    extra: {
        socketPath: "/Applications/MAMP/tmp/mysql/mysql.sock"
    },
    synchronize: true,
    logging: false,
    entities: [
        User, Brand, Category, Product, Store, Outproduct, Itemoutproduct
    ],
}).then(async connection => {

    // create express app
    const app = express();

    app.use(cors());
    app.use(helmet());
    app.use(bodyParser.json());

    app.use("/", routes);

    // app.use(bodyParser.json());

    // register express routes from defined application routes
    /*Routes.forEach(route => {
        (app as any)[route.method](route.route, (req: Request, res: Response, next: Function) => {
            const result = (new (route.controller as any))[route.action](req, res, next);
            if (result instanceof Promise) {
                result.then(result => result !== null && result !== undefined ? res.send(result) : undefined);

            } else if (result !== null && result !== undefined) {
                res.json(result);
            }
        });
    });*/

    // setup express app here
    // ...

    // start express server
    app.listen(3001, () => {
        console.log("Server started on port 3001!");
    });

}).catch(error => console.log(error));
