import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, CreateDateColumn, OneToMany, OneToOne, JoinColumn } from 'typeorm';
import { Product } from './product.entity';
import { Outproduct } from './outproduct';

@Entity()
export class Itemoutproduct {
  @PrimaryGeneratedColumn()
  id: number;
  
  @Column()
  product: number;

  @Column()
  quantity: number;

  @ManyToOne(type => Outproduct, out => out.products, {onDelete: "CASCADE"})
  out: Outproduct;

}