import { Entity, Column, PrimaryGeneratedColumn, OneToMany, Unique } from 'typeorm';
import { Product } from './product.entity';

@Entity()
@Unique(["name"])
export class Brand {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 50 })
  name: string;

  @Column()
  status: string;

  @OneToMany(type => Product, product => product.brand)
  products: Product[];
}