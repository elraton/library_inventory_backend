import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { Category } from './category.entity';
import { Brand } from './brand.entity';
import { Store } from './store.entity';
import { Outproduct } from './outproduct';

@Entity()
export class Product {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  image: string;
  
  @Column({ length: 50 })
  name: string;

  @Column()
  lot: number;

  @Column({ length: 50 })
  code: string;

  @Column("decimal", { precision: 5, scale: 2 })
  price: number;

  @Column("decimal", { precision: 5, scale: 2 })
  unit_price: number;

  @Column()
  quantity: number;

  @Column()
  quantity_actual: number;

  @Column()
  description: string;
  
  @ManyToOne(type => Category, category => category.products)
  category: Category;

  @ManyToOne(type => Brand, brand => brand.products)
  brand: Brand;

  @ManyToOne(type => Store, store => store.products)
  store: Store;

}