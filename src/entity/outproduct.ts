import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, CreateDateColumn, OneToMany } from 'typeorm';
import { Itemoutproduct } from './itemout';

@Entity()
export class Outproduct {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  status: string;
  
  @CreateDateColumn({type: "timestamp"})
  createdAt: Date;

  @OneToMany(type => Itemoutproduct, item => item.out, {onDelete: "CASCADE"})
  products: Itemoutproduct[];

}