import { Router } from "express";
import StoreController from "../controller/StoreController";
import { checkJwt } from "../middlewares/checkJwt";
import { checkRole } from "../middlewares/checkRole";

const router = Router();

router.get("/", [checkJwt], StoreController.listAll);
router.post("/", [checkJwt, checkRole(["ADMIN"])], StoreController.create);
router.put("/:id", [checkJwt, checkRole(["ADMIN"])], StoreController.update);
router.delete("/:id", [checkJwt, checkRole(["ADMIN"])], StoreController.delete);

export default router;