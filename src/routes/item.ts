import { Router } from "express";
import { checkJwt } from "../middlewares/checkJwt";
import ItemOutController from "../controller/ItemOut";

const router = Router();

router.put("/:id", [checkJwt], ItemOutController.update);
router.delete("/:id", [checkJwt], ItemOutController.delete);

export default router;