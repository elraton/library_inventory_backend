import { Router } from "express";
import BrandController from "../controller/BrandController";
import { checkJwt } from "../middlewares/checkJwt";
import { checkRole } from "../middlewares/checkRole";

const router = Router();

router.get("/", [checkJwt], BrandController.listAll);
router.post("/", [checkJwt, checkRole(["ADMIN"])], BrandController.create);
router.put("/:id", [checkJwt, checkRole(["ADMIN"])], BrandController.update);
router.delete("/:id", [checkJwt, checkRole(["ADMIN"])], BrandController.delete);

export default router;