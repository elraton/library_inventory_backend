import { Router } from "express";
import ReportsController from "../controller/ReportsController";
import { checkJwt } from "../middlewares/checkJwt";
import { checkRole } from "../middlewares/checkRole";

const router = Router();

router.get("/all", [checkJwt, checkRole(["ADMIN"])], ReportsController.listAll);
router.get("/lower", [checkJwt, checkRole(["ADMIN"])], ReportsController.listQuantityLowers);

export default router;