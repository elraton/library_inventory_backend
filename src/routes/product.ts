import { Router } from "express";
import ProductController from "../controller/ProductController";
import { checkJwt } from "../middlewares/checkJwt";
import { checkRole } from "../middlewares/checkRole";

const router = Router();

router.get("/", [checkJwt], ProductController.listAll);
router.post("/", [checkJwt, checkRole(["ADMIN"])], ProductController.create);
router.put("/:id", [checkJwt, checkRole(["ADMIN"])], ProductController.update);
router.delete("/:id", [checkJwt, checkRole(["ADMIN"])], ProductController.delete);

export default router;