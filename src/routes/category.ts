import { Router } from "express";
import CategoryController from "../controller/CategoryController";
import { checkJwt } from "../middlewares/checkJwt";
import { checkRole } from "../middlewares/checkRole";

const router = Router();

router.get("/", [checkJwt], CategoryController.listAll);
router.post("/", [checkJwt, checkRole(["ADMIN"])], CategoryController.create);
router.put("/:id", [checkJwt, checkRole(["ADMIN"])], CategoryController.update);
router.delete("/:id", [checkJwt, checkRole(["ADMIN"])], CategoryController.delete);

export default router;