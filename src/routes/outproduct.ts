import { Router } from "express";
import { checkJwt } from "../middlewares/checkJwt";
import OutProductController from "../controller/OutproductController";

const router = Router();

router.get("/", [checkJwt], OutProductController.listAll);
router.post("/", [checkJwt], OutProductController.create);
router.delete("/:id", [checkJwt], OutProductController.delete);

export default router;