import { Router, Request, Response } from "express";
import auth from "./auth";
import user from "./user";

import category from "./category";
import brand from "./brand";
import store from "./store";
import product from "./product";
import outproduct from "./outproduct";
import item from "./item";
import reports from "./reports";

const routes = Router();

routes.use("/auth", auth);
routes.use("/user", user);
routes.use("/category", category);
routes.use("/brand", brand);
routes.use("/store", store);
routes.use("/product", product);
routes.use("/outproduct", outproduct);
routes.use("/item", item);
routes.use("/report", reports);

export default routes;