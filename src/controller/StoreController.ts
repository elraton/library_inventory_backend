import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { validate } from "class-validator";
import { Store } from "../entity/store.entity";

class StoreController{

    static listAll = async (req: Request, res: Response) => {
        const repository = getRepository(Store);
        const data = await repository.find({
            select: ["id", "name", "status"],
            relations: ["products"]
        });

        res.send(data);
    };

    static create = async (req: Request, res: Response) => {
        let { name, status } = req.body;
        let data = new Store();

        data.name = name;
        data.status = status;

        const errors = await validate(data);
        if (errors.length > 0) {
            res.status(400).send(errors);
            return;
        }

        const repository = getRepository(Store);
        try {
            await repository.save(data);
        } catch (e) {
            res.status(409).send("store name already in use");
            return;
        }

        //If all ok, send 201 response
        res.status(201).send(data);
    };

    static update = async (req: Request, res: Response) => {
        const id = req.params.id;

        const { name, status } = req.body;

        const repository = getRepository(Store);
        let data;
        try {
            data = await repository.findOneOrFail(id);
        } catch (error) {
            //If not found, send a 404 response
            res.status(404).send("Store not found");
            return;
        }

        //Validate the new values on model
        data.name = name;
        data.status = status;
        const errors = await validate(data);
        if (errors.length > 0) {
            res.status(400).send(errors);
            return;
        }

        //Try to safe, if fails, that means username already in use
        try {
            await repository.save(data);
        } catch (e) {
            res.status(409).send("Store name already in use");
            return;
        }

        //After all send a 204 (no content, but accepted) response
        res.status(204).send(data);
    };

    static delete = async (req: Request, res: Response) => {
        const id = req.params.id;

        const repository = getRepository(Store);
        let data: Store;
        try {
            data = await repository.findOneOrFail(id);
        } catch (error) {
            res.status(404).send("Store not found");
            return;
        }
        repository.delete(id);

        res.status(204).send();
    };
};

export default StoreController;