import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { validate } from "class-validator";
import { Itemoutproduct } from "../entity/itemout";
import { Product } from "../entity/product.entity";

class ItemOutController{

    static update = async (req: Request, res: Response) => {
        const id = req.params.id;

        const { product, quantity } = req.body;

        const repository = getRepository(Itemoutproduct);
        let data;
        try {
            data = await repository.findOneOrFail(id);
        } catch (error) {
            //If not found, send a 404 response
            res.status(404).send("Itemoutproduct not found");
            return;
        }

        //Validate the new values on model
        data.product = product;
        data.quantity = quantity;
        const errors = await validate(data);
        if (errors.length > 0) {
            res.status(400).send(errors);
            return;
        }

        //Try to safe, if fails, that means username already in use
        try {
            await repository.save(data);
        } catch (e) {
            res.status(409).send("Itemoutproduct name already in use");
            return;
        }

        //After all send a 204 (no content, but accepted) response
        res.status(204).send(data);
    };

    static delete = async (req: Request, res: Response) => {
        const id = req.params.id;

        const repository = getRepository(Itemoutproduct);
        let data: Itemoutproduct;
        try {
            data = await repository.findOneOrFail(id);
        } catch (error) {
            res.status(404).send("Itemoutproduct not found");
            return;
        }
        repository.delete(id);

        res.status(204).send();
    };
};

export default ItemOutController;