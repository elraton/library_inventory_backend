import { Request, Response } from "express";
import { getRepository, LessThanOrEqual } from "typeorm";
import { validate } from "class-validator";
import { Product } from "../entity/product.entity";

class ReportController{

    static listAll = async (req: Request, res: Response) => {
        const repository = getRepository(Product);
        const data = await repository.find({
            select: ["id", "image", "name", "code", "price", "unit_price", "quantity", "quantity_actual", "description"],
            relations: ["category", "brand", "store"]
        });

        res.send(data);
    };

    static listQuantityLowers = async (req: Request, res: Response) => {
        const repository = getRepository(Product);
        const data = await repository.find({
            select: [ "id", "image", "name", "code", "price", "unit_price", "quantity", "quantity_actual", "description" ],
            relations: [ "category", "brand", "store" ],
            order: { brand: "ASC" }
        });

        const data2 = data.filter( (x) => {
            if ( Number(x.quantity_actual) <= (Number(x.quantity) * 0.3)) {
                return x;
            }
        });

        res.send(data2);
    };
};

export default ReportController;