import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { validate } from "class-validator";
import { Outproduct } from "../entity/outproduct";
import { Product } from "../entity/product.entity";
import { Itemoutproduct } from "../entity/itemout";

class OutProductController{

    static listAll = async (req: Request, res: Response) => {
        const repository = getRepository(Outproduct);
        const data = await repository.find({
            select: ["id", "createdAt", "status"],
            relations: ["products"]
        });

        res.send(data);
    };

    static create = async (req: Request, res: Response) => {
        let { products, status } = req.body;

        const productRepository = getRepository(Product);
        const itemOutRepository = getRepository(Itemoutproduct);
        const repository = getRepository(Outproduct);

        let item;

        let data = new Outproduct();
        data.status = status;
        let data_saved;
        try {
            data_saved = await repository.save(data);
        } catch (e) {
            res.status(409).send("Outproduct name already in use");
            return;
        }

        for ( const xx of products ) {
            item = new Itemoutproduct();
            item.product = xx.product;
            item.quantity = xx.quantity;
            item.out = data_saved;
            await itemOutRepository.save(item);
        }

        //If all ok, send 201 response
        res.status(201).send(data);
    };

    static delete = async (req: Request, res: Response) => {
        const id = req.params.id;

        const repository = getRepository(Outproduct);
        let data: Outproduct;
        try {
            data = await repository.findOneOrFail(id);
        } catch (error) {
            res.status(404).send("Outproduct not found");
            return;
        }
        data.status = 'CANCELADO';
        await repository.save(data);

        res.status(204).send();
    };
};

export default OutProductController;