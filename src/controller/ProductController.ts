import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { validate } from "class-validator";
import { Product } from "../entity/product.entity";
import { Brand } from "../entity/brand.entity";
import { Category } from "../entity/category.entity";
import { Store } from "../entity/store.entity";

class ProductController{

    static listAll = async (req: Request, res: Response) => {
        const repository = getRepository(Product);
        const data = await repository.find({
            select: ["id", "image", "name", "code", "price", "unit_price", "quantity", "lot", "quantity_actual", "description"],
            relations: ["category", "brand", "store"]
        });

        res.send(data);
    };

    static create = async (req: Request, res: Response) => {
        let { name, image, code, price, unit_price, quantity, lot, quantity_actual, description, category, brand, store } = req.body;

        const brandRepository = getRepository(Brand);
        const brandEntity = await brandRepository.findOneOrFail(brand);

        const categoryRepository = getRepository(Category);
        const categoryEntity = await categoryRepository.findOneOrFail(category);

        const storeRepository = getRepository(Store);
        const storeEntity = await storeRepository.findOneOrFail(store);


        let data = new Product();
        data.name = name;
        data.image = image;
        data.code = code;
        data.lot = lot;
        data.price = price;
        data.unit_price = unit_price;
        data.quantity = quantity;
        data.quantity_actual = quantity_actual;
        data.description = description;
        data.brand = brandEntity;
        data.category = categoryEntity;
        data.store = storeEntity;

        const errors = await validate(data);
        if (errors.length > 0) {
            res.status(400).send(errors);
            return;
        }

        const repository = getRepository(Product);
        try {
            await repository.save(data);
        } catch (e) {
            res.status(409).send("Product cannot be saved");
            return;
        }

        //If all ok, send 201 response
        res.status(201).send(data);
    };

    static update = async (req: Request, res: Response) => {
        const id = req.params.id;

        const { name, image, code, price, unit_price, quantity, lot, quantity_actual, description, category, brand, store } = req.body;

        const brandRepository = getRepository(Brand);
        const brandEntity = await brandRepository.findOneOrFail(brand);

        const categoryRepository = getRepository(Category);
        const categoryEntity = await categoryRepository.findOneOrFail(category);

        const storeRepository = getRepository(Store);
        const storeEntity = await storeRepository.findOneOrFail(store);

        const repository = getRepository(Product);
        let data;
        try {
            data = await repository.findOneOrFail(id);
        } catch (error) {
            //If not found, send a 404 response
            res.status(404).send("Brand not found");
            return;
        }

        //Validate the new values on model
        data.name = name;
        data.image = image;
        data.code = code;
        data.price = price;
        data.lot = lot;
        data.unit_price = unit_price;
        data.quantity = quantity;
        data.quantity_actual = quantity_actual;
        data.description = description;
        data.brand = brandEntity;
        data.category = categoryEntity;
        data.store = storeEntity;
        const errors = await validate(data);
        if (errors.length > 0) {
            res.status(400).send(errors);
            return;
        }

        //Try to safe, if fails, that means username already in use
        try {
            await repository.save(data);
        } catch (e) {
            res.status(409).send("Product cannot be saved");
            return;
        }

        //After all send a 204 (no content, but accepted) response
        res.status(204).send(data);
    };

    static delete = async (req: Request, res: Response) => {
        const id = req.params.id;

        const repository = getRepository(Product);
        let data: Product;
        try {
            data = await repository.findOneOrFail(id);
        } catch (error) {
            res.status(404).send("Product not found");
            return;
        }
        repository.delete(id);

        res.status(204).send();
    };
};

export default ProductController;