import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { validate } from "class-validator";
import { Brand } from "../entity/brand.entity";

class BrandController{

    static listAll = async (req: Request, res: Response) => {
        const repository = getRepository(Brand);
        const data = await repository.find({
            select: ["id", "name", "status"],
            relations: ["products"]
        });

        res.send(data);
    };

    static create = async (req: Request, res: Response) => {
        let { name, status } = req.body;
        let data = new Brand();

        data.name = name;
        data.status = status;

        const errors = await validate(data);
        if (errors.length > 0) {
            res.status(400).send(errors);
            return;
        }

        const repository = getRepository(Brand);
        try {
            await repository.save(data);
        } catch (e) {
            res.status(409).send("Brand name already in use");
            return;
        }

        //If all ok, send 201 response
        res.status(201).send(data);
    };

    static update = async (req: Request, res: Response) => {
        const id = req.params.id;

        const { name, status } = req.body;

        const repository = getRepository(Brand);
        let data;
        try {
            data = await repository.findOneOrFail(id);
        } catch (error) {
            //If not found, send a 404 response
            res.status(404).send("Brand not found");
            return;
        }

        //Validate the new values on model
        data.name = name;
        data.status = status;
        const errors = await validate(data);
        if (errors.length > 0) {
            res.status(400).send(errors);
            return;
        }

        //Try to safe, if fails, that means username already in use
        try {
            await repository.save(data);
        } catch (e) {
            res.status(409).send("Brand name already in use");
            return;
        }

        //After all send a 204 (no content, but accepted) response
        res.status(204).send(data);
    };

    static delete = async (req: Request, res: Response) => {
        const id = req.params.id;

        const repository = getRepository(Brand);
        let data: Brand;
        try {
            data = await repository.findOneOrFail(id);
        } catch (error) {
            res.status(404).send("Brand not found");
            return;
        }
        repository.delete(id);

        res.status(204).send();
    };
};

export default BrandController;